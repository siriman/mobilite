package models;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe representant le monde
 */
public class Monde {

    /**
     * La liste des entites
     */
    private List<EntiteMobile> monde2D;

    /**
     * Constructeur logique
     */
    public Monde(){
        this.monde2D = new ArrayList<EntiteMobile>();
    }

    /**
     * Ajoute une entite dans le monde
     *
     * @param entiteMobile une entite mobile, voir {@link EntiteMobile}
     */
    public void addEntiteMobile(EntiteMobile entiteMobile){
        this.monde2D.add(entiteMobile);
    }

    /**
     * Lance n fois le deplacement des entites mobiles du monde.
     *
     * @param iteration int, le nombre d'iteration
     */
    public void lancerIterarions(int iteration){
        for (int i = 0; i < iteration; i++) {
            for(EntiteMobile entiteMobile : monde2D){
                entiteMobile.deplace();
                System.out.println("[ ITERATION : "+ i +" ] ENTITE AVEC LE COMPORTEMENT : "+entiteMobile.getComportement().toString()+" A LA "+ entiteMobile.getPosition().toString());
            }
            System.out.println("\n------------ NOMBRE COLLISION POUR ITERATION : "+ i + " EST EGALE A "+ collisions() +" ---------------\n");
        }
    }

    /**
     * Calcule le nombre d'iteration a un instant donne
     *
     * @return int, le nombre de collision
     */
    public int collisions(){
        int nombreCollision = 0;
        for (int i = 0; i < monde2D.size()-1; i++) {
            for (int j = 1; j < monde2D.size(); j++) {
                EntiteMobile entiteMobileI = monde2D.get(i);
                EntiteMobile entiteMobileJ = monde2D.get(j);
                if(entiteMobileI.getPosition().equals(entiteMobileJ.getPosition()))
                    nombreCollision += 1;
            }
        }
        return nombreCollision;
    }
}
