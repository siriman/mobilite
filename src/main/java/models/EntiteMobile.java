package models;

/**
 * Classe representant une entite mobile
 */
public class EntiteMobile {

    /**
     * Le comportement associe a l'entite
     */
    protected Comportement comportement;

    /**
     * La possition associe a l'entite
     */
    protected Position position;

    /**
     * Constructeur logique
     *
     * @param comportement la valeur de {@link EntiteMobile#comportement}
     * @param position la valeur de {@link EntiteMobile#position}
     */
    public EntiteMobile(Comportement comportement, Position position) {
        this.comportement = comportement;
        this.position = position;
    }

    /**
     * Accesseur.
     *
     * @return la valeur de {@link EntiteMobile#comportement}.
     */
    public Comportement getComportement() {
        return comportement;
    }

    /**
     * Accesseur
     *
     * @param comportement la valeur de {@link EntiteMobile#comportement}
     */
    public void setComportement(Comportement comportement) {
        this.comportement = comportement;
    }

    /**
     * Accesseur.
     *
     * @return la valeur de {@link EntiteMobile#position}.
     */
    public Position getPosition() {
        return position;
    }

    /**
     * Accesseur
     *
     * @param position la valeur de {@link EntiteMobile#position}
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     *
     */
    public void deplace(){
        setPosition(comportement.getProchainMouvement(this));
    }
}
