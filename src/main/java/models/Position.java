package models;

/**
 * Classe representant la position ie le deplacement d'une entite mobile dans le monde
 */
public class Position {

    /**
     * Le numero de ligne
     */
    protected final int ligne;

    /**
     * Le numero de colonne
     */
    protected final int colonne;

    /**
     * Constructeur logique
     *
     * @param ligne la valeur de {@link Position#ligne}
     * @param colonne la valeur de {@link Position#colonne}
     */
    public Position(int ligne, int colonne) {
        this.ligne = ligne;
        this.colonne = colonne;
    }

    /**
     * Accesseur
     *
     * @return la valeur de {@link Position#ligne}
     */
    public int getLigne() {
        return ligne;
    }

    /**
     * Acceseur
     *
     * @return la valeur de {@link Position#colonne}
     */
    public int getColonne() {
        return colonne;
    }

    /**
     * Retourne la voisine de cette position selon la direction fournie en
     * argument.
     *
     * @param direction la direction.
     * @return la voisine de cette position selon la direction fournie en
     *   argument.
     */
    public Position suivante(Direction direction) {
        return new Position(ligne + direction.lireDeltaLigne(),
                colonne + direction.lireDeltaColonne());
    }

    @Override
    public String toString() {
        return "Position { " +
                "ligne=" + ligne +
                ", colonne=" + colonne +
                " }";
    }

    @Override
    public boolean equals(Object object){
        if(object == null)
            return false;
        Position position = (Position) object;
        return ligne == position.getLigne() && colonne == position.getColonne();
    }
}
