package models;

/**
 * Classe representant un comportement cyclique.
 *
 * Elle implemente le behavioral design pattern Strategy.
 */
public class ComportementCyclique implements Comportement {

    /**
     * La position qui represente le deplacement initial.
     */
    protected Position initiale;

    /**
     * La position represente le deplacement final.
     */
    protected Position finale;

    /**
     * La position courante.
     */
    protected Position courante;

    /**
     * Constructeur logique.
     *
     * @param initiale la valeur de {@link ComportementCyclique#initiale}
     * @param finale la valeur de {@link ComportementCyclique#finale}
     */
    public ComportementCyclique(Position initiale, Position finale) {
        this.initiale = new Position(initiale.getLigne(), initiale.getColonne());
        this.finale = new Position(finale.getLigne(), finale.getColonne());
        this.courante = this.initiale;
    }

    /**
     * Constructeur de recopie.
     *
     * @param comportementCyclique un comportement cyclique.
     */
    public ComportementCyclique(ComportementCyclique comportementCyclique) {
        this(comportementCyclique.getInitiale(), comportementCyclique.getFinale());
    }

    /**
     * Accesseur.
     *
     * @return la valeur de {@link ComportementCyclique#initiale}
     */
    public Position getInitiale() {
        return initiale;
    }

    /**
     * Accesseur.
     *
     * @return la valeur de {@link ComportementCyclique#finale}
     */
    public Position getFinale() {
        return finale;
    }

    /**
     * Renvoie le prochain mouvement de l'entite passe en parametre
     *
     * @param entiteMobile une entite mobile
     * @return la prochaine position de l'entite.
     */
    public Position getProchainMouvement(EntiteMobile entiteMobile) {
        /*
        if ( courante.equals(initiale) ){
            courante = finale;
            return new Position(finale.getLigne(), finale.getColonne());
        }
        courante = initiale;
        return new Position(initiale.getLigne(), initiale.getColonne()); */
        entiteMobile.setComportement(creerComportementInverse());
        return finale;
    }

    /**
     * Creer un comportement inverse au comportement initial de la classe.
     *
     * @return un comportement inverse a l'initial.
     */
    public ComportementCyclique creerComportementInverse() {
        return new ComportementCyclique(finale, initiale);
    }

    @Override
    public String toString(){
        return "CYCLIQUE";
    }
}
