package models;

/**
 * Classe representant une direction
 */
public enum Direction {

    HAUT(+1, 0),      /** 0) Direction HAUT.       */
    DROITE(0, +1),     /** 1) Direction DROITE.     */
    BAS(-1, 0),        /** 2) Direction BAS.        */
    GAUCHE(0, -1);     /** 3) Direction GAUCHE.     */

    /**
     * Accesseur.
     *
     * @return la valeur de {@link Direction#deltaLigne}.
     */
    public int lireDeltaLigne() {
        return deltaLigne;
    }

    /**
     * Accesseur.
     *
     * @return la valeur de {@link Direction#deltaColonne}.
     */
    public int lireDeltaColonne() {
        return deltaColonne;
    }

    /**
     * Retourne le successeur de cette direction dans l'enumeration.
     *
     * @return le successeur de cette direction dans l'enumeration.
     */
    public Direction suivante() {
        return values()[(ordinal() + 1) % values().length];
    }

    /**
     * Retourne la direction opposee de cette direction dans l'enumeration.
     *
     * @return la direction opposee de cette direction dans l'enumeration.
     */
    public Direction opposee() {
        return values()[(ordinal() + 4) % values().length];
    }

    /**
     * Constructeur logique.
     *
     * @param deltaLigne la valeur de {@link Direction#deltaLigne}.
     * @param deltaColonne la valeur de {@link Direction#deltaColonne}.
     */
    private Direction(int deltaLigne, int deltaColonne) {
        this.deltaLigne = deltaLigne;
        this.deltaColonne = deltaColonne;
    }

    /**
     * Delta sur les lignes.
     */
    private int deltaLigne;

    /**
     * Delta sur les colonnes.
     */
    private int deltaColonne;
}
