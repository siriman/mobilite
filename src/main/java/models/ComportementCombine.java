package models;

/**
 * Classe representant un comportement combine
 *
 * Elle implemente le behavioral design pattern Strategy
 */
public class ComportementCombine implements Comportement {

    /**
     * Comportement a combiner associe la classe
     */
    public Comportement unComportement;

    /**
     * Un autre comportement a combiner associe a la classe
     */
    public Comportement unAutreComportement;


    /**
     * Constructeur logique
     *
     * @param unComportement la valeur de {@link ComportementCombine#unComportement}
     * @param unAutreComportement la valeur de {@link ComportementCombine#unAutreComportement}
     */
    public ComportementCombine(Comportement unComportement, Comportement unAutreComportement) {
        this.unComportement = unComportement;
        this.unAutreComportement = unAutreComportement;
    }

    /**
     * Renvoie le prochain mouvement de l'entite passe en parametre
     *
     * @param entiteMobile une entite mobile
     * @return la prochaine position de l'entite.
     */
    public Position getProchainMouvement(EntiteMobile entiteMobile) {
        /** Somme vectorielle de U(xu, yu) avec V(xv, xv) est egale (u + v) = (xu + xv, yu + yv) */

        /** Coord. du vecteur u = (xB - XA, yB - yA) **/
        Position suivantUnComportement = unComportement.getProchainMouvement(entiteMobile);
        int xU = suivantUnComportement.getLigne() - entiteMobile.getPosition().getLigne();
        int yU = suivantUnComportement.getColonne() - entiteMobile.getPosition().getColonne();

        /** Coord. du vecteur v = (xC - XA, yC - yA) **/
        Position suivantUnAutreComportement = unAutreComportement.getProchainMouvement(entiteMobile);
        int xV = suivantUnAutreComportement.getLigne() - entiteMobile.getPosition().getLigne();
        int yV = suivantUnAutreComportement.getColonne() - entiteMobile.getPosition().getColonne();

        int xUV = xU + xV;
        int yUV = yU + yV;

        int ligne = xUV + entiteMobile.getPosition().getLigne();
        int colonne = yUV + entiteMobile.getPosition().getColonne();

        return new Position(ligne, colonne);
    }

    @Override
    public String toString(){
        return "COMBINE";
    }
}
