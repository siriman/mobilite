package models;

/**
 * Classe representant un comportement
 */
interface Comportement {

    /**
     * Renvoie le prochain mouvement de l'entite passe en parametre
     *
     * @param entiteMobile une entite mobile
     * @return la prochaine position de l'entite.
     */
    Position getProchainMouvement(EntiteMobile entiteMobile);
}
