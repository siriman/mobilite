package models;


/**
 * Classe representant un comportement tout droit.
 *
 * Elle implemente le behavioral design pattern Strategy.
 */
public class ComportementToutDroit implements Comportement {

    /**
     * La direction de l'entite.
     */
    protected Direction direction;

    /**
     * Constructeur logique.
     *
     * @param direction la valeur de {@link ComportementToutDroit#direction}
     */
    public ComportementToutDroit(Direction direction) {
        this.direction = direction;
    }

    /**
     * Renvoie le prochain mouvement de l'entite passe en parametre
     *
     * @param entiteMobile une entite mobile
     * @return la prochaine position de l'entite.
     */
    public Position getProchainMouvement(EntiteMobile entiteMobile) {
        return entiteMobile.getPosition().suivante(direction);
    }

    @Override
    public String toString(){
        return "TOUT_DROIT";
    }
}
