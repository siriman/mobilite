package models;

/**
 * La classe main de l'application
 */
public class Main {

    /**
     * Le programme principal
     *
     * @param args les arguments
     */
    public static void main(String [] args){

        /** Demonstration */

        Position position = new Position(1, 2);
        Comportement comportementToutDroit = new ComportementToutDroit(Direction.HAUT);
        Comportement comportementAleatoire = new ComportementAleatoire(10, 1);
        Comportement comportementCombine = new ComportementCombine(comportementToutDroit, comportementAleatoire);
        Comportement comportementCyclique = new ComportementCyclique(new Position(-1, 0), new Position(1, 0));

        EntiteMobile entiteMobile = new EntiteMobile(comportementToutDroit, position);
        EntiteMobile entiteMobile1 = new EntiteMobile(comportementAleatoire, position);
        EntiteMobile entiteMobile2 = new EntiteMobile(comportementCombine, position);
        EntiteMobile entiteMobile3 = new EntiteMobile(comportementCyclique, new Position(-1, 0));

        Monde monde = new Monde();
        monde.addEntiteMobile(entiteMobile);
        monde.addEntiteMobile(entiteMobile1);
        monde.addEntiteMobile(entiteMobile2);
        monde.addEntiteMobile(entiteMobile3);
        monde.lancerIterarions(20);
    }
}
