package models;

/**
 * Classe representant un comportement aleatoire
 *
 * Elle implemente le behavioral design pattern Strategy
 */
public class ComportementAleatoire implements Comportement{

    /**
     * La valeur maximum
     */
    private int valeurMax;

    /**
     * La valeur minimum
     */
    private int valeurMin;

    /**
     * Constructeur logique
     *
     * @param valeurMax la valeur de {@link ComportementAleatoire#valeurMax}
     * @param valeurMin la valeur de {@link ComportementAleatoire#valeurMin}
     */
    public ComportementAleatoire(int valeurMax, int valeurMin){
        this.valeurMax = valeurMax;
        this.valeurMin = valeurMin;
    }

    /**
     * Renvoie le prochain mouvement de l'entite passe en parametre
     *
     * @param entiteMobile une entite mobile
     * @return la prochaine position de l'entite.
     */
    public Position getProchainMouvement(EntiteMobile entiteMobile) {
        int ligne = valeurMin + (int) (Math.random() * (valeurMax - valeurMin));
        int colonne = valeurMin + (int) (Math.random() * (valeurMax - valeurMin));
        return new Position(ligne, colonne);
    }

    @Override
    public String toString(){
        return "ALEATOIRE";
    }
}
