**Projet du module POO JAVA**

Projet de simulation du déplacement des entités mobiles dans un monde deux à 
dimension avec implémentation du `Pattern Strategie`. Comme différents types de déplacement, nous avons le déplacement tout droit, le déplacement cyclique etc.



**Auteurs**
Siriman TRAORE - 21308390
Lofti IDIR - 21613320

**Pré-réquis**
Il peut être comode d'avoir maven pour la génération du jar

**Génération du jar**
Se placer dans le dossier du projet « mobilite » et lancer `mvn package`

**Exécution et test**

     java -classpath target/mobilite.jar models.Main
